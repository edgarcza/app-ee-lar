<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('proyecto');
});

Route::get('/prueba', 'UserController@Prueba');
Route::post('/not', 'UserController@Not');
Route::get('/ws', function () {
    $output = [];
    \Artisan::call('websockets:serve', $output);
    dd($output);
});
Route::get('/prueba2', 'InfoController@Push');

Route::get('/api', 'UserController@Prueba');
Route::post('/user', 'UserController@Get');
Route::post('/user/byl', 'UserController@ByL');
Route::post('/user/sales', 'UserController@Sales');
Route::post('/user/create', 'UserController@Create');
Route::post('/user/login', 'UserController@Login');
Route::post('/user/fb', 'UserController@FB');
Route::post('/user/session', 'UserController@Session');
Route::post('/user/save', 'UserController@Save');
Route::post('/user/device', 'UserController@Device');
Route::post('/user/avatar', 'UserController@Avatar');
Route::post('/user/nots', 'UserController@NotsConfig');
Route::post('/user/nots/save', 'UserController@NotsConfigSave');

Route::post('/post/create', 'PostController@Create');
Route::post('/posts', 'PostController@Posts');
Route::post('/posts/profile', 'PostController@PostsProfile');
Route::post('/post/like', 'PostController@Like');
Route::post('/post/comment', 'PostController@Comment');
Route::post('/post/comments', 'PostController@Comments');

Route::post('/messages', 'MessageController@Messages');
Route::post('/messages/chat', 'MessageController@Chat');
Route::post('/messages/chat/new', 'MessageController@ChatNew');
Route::post('/messages/chat/send', 'MessageController@Send');

Route::post('/infos/user', 'InfoController@InfoUser');

Route::post('/notifications/user', 'NotificationController@NotsUser');

Route::post('/vault/files', 'VaultFileController@Files');
Route::post('/vault/file', 'VaultFileController@File');

Route::post('/shop/load', 'ShopItemController@LoadView');
Route::post('/shop/item-details', 'ShopItemController@ItemDetails');
Route::post('/shop/add-to-cart', 'ShopItemController@AddToCart');
Route::post('/shop/cart-items', 'ShopItemController@CartItems');
Route::post('/shop/cart-item/delete', 'ShopItemController@CartItemDelete');

Route::post('/paymethod/add', 'UserPaymethodController@Add');
Route::post('/paymethods/get', 'UserPaymethodController@Get');
Route::post('/paymethod/delete', 'UserPaymethodController@DeleteMethod');
