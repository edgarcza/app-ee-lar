<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>DATO</th>
    </tr>
    </thead>
    <tbody>
    @foreach($datos as $dato)
        <tr>
            <td>{{ $dato['id'] }}</td>
            <td>{{ $dato['dato'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>