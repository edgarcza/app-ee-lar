<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Base
{
    protected $table = 'messages';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            ['users', 'users.id', 'messages.user_id'],
            ['messages_groups', 'messages_groups.id', 'message.message_group_id'],
        ];
    }
}
