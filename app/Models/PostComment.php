<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Base
{
    protected $table = 'posts_comments';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            ['users', 'users.id', 'posts_comments.user_id'],
            ['posts', 'posts.id', 'posts_comments.post_id'],
        ];
    }
}
