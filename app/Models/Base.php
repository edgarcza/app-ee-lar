<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

use App\Exports\TablaExport;
// use App\Imports\TablaImport;
use Maatwebsite\Excel\Facades\Excel;

class Base extends Model
{
    protected $guarded = ['id'];

    public $Datos;
    public $Nuevo = false;
    public $Joins;
    public $Orders;

    public $F_Datos;
    public $F_Datos_Total;

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }

    public function _Save($datos)
    {
        $Guardar = $datos;
        $Guardado = null;

        if (isset($Guardar['id'])) {
            $Guardado = $this::find($Guardar['id']);
            $Guardado->fill($Guardar);
            $Guardado->save();
            $this->Nuevo = false;
            $this->Datos = $Guardado;
        } else {
            $Guardado = $this::create($Guardar);
            $this->Nuevo = true;
            $this->Datos = $Guardado;
        }

        if (!empty($Guardado))
            return true;
        return false;
    }

    public function _Delete($id)
    {
        $ID = $id;

        $Eliminado = $this::where('id', $ID)->delete();
        // $Usuario;

        if (!empty($Eliminado))
            return true;
        return false;
    }

    public function Filter($filtros = [], $paginador = null, $select = null, $joins = null)
    {
        // $Filtros = $datos['Filtros'];
        // $Paginador = (isset($datos['Paginador'])) ?
        //     $datos['Paginador'] : null;
        $Filtros = $filtros;
        $Paginador = $paginador;

        $Query = $this::query();

        $JoinsR = (isset($joins)) ? $joins : $this->Joins;

        if (!empty($JoinsR)) {
            foreach ($JoinsR as $Join) {
                $Query->join($Join[0], $Join[1], $Join[2]);
            }
        }

        if (!empty($this->Orders)) {
            foreach ($this->Orders as $Order) {
                $Query->orderBy($Order[0], $Order[1]);
            }
        }

        foreach ($Filtros as $Filtro) {
            if (isset($Filtro['cond']) && !empty($Filtro['cond']))
                $Query = $Query->where($Filtro['field'], $Filtro['cond'], $Filtro['value']);
            else
                $Query = $Query->where($Filtro['field'], 'like', '%' . $Filtro['value'] . '%');
        }

        if (!isset($select))
            $Query = $Query->select('*', (new $this)->getTable() . '.id as main_id');
        else
            $Query = $Query->select(DB::raw($select), (new $this)->getTable() . '.id as main_id');

        $DatosUL = $Query->get()->count();

        if (!empty($Paginador)) {
            $Query = $Query->offset($Paginador['Cantidad'] * $Paginador['Pagina']);
            $Query = $Query->limit($Paginador['Cantidad']);
        }
        $Datos = $Query->get();

        $this->F_Datos = $Datos;
        $this->F_Datos_Total = $DatosUL;

        return $Datos;

        // return response()->json(['proceso' => true, 'datos' => $Datos, 'total' => $DatosUL, 'req' => $request->all()['datos'], 'joins' => $this->Joins]);
    }

    public function Prepare()
    {
        $Query = new $this;
        $Query->select('*', (new $this)->getTable().'.id as main_id');

        foreach ($this->Joins as $Join) {
            $Query->join($Join[0], $Join[1], $Join[2]);
        }

        return $Query;
    }

    public function Excel($campos, $filas)
    {

        $className = get_class($this);
        $baseClass = class_basename($className);

        $AN = $baseClass . ' ' . date("d-m-Y H:i:s") . '.xlsx';

        Excel::store(new TablaExport($campos, $filas), 'tablas/' . $AN);

        return $AN;
        // return $campos;
    }
}
