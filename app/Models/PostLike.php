<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostLike extends Base
{
    protected $table = 'posts_likes';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            ['users', 'users.id', 'posts_likes.user_id'],
            ['posts', 'posts.id', 'posts_likes.post_id'],
        ];
    }
}
