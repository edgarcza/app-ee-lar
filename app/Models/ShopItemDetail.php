<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopItemDetail extends Base
{
    protected $table = 'shop_items_details';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            ['shop_items', 'shop_items.id', 'shop_items_details.shop_id']
        ];
    }
}
