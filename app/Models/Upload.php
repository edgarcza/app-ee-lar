<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

class Upload
{
    public $Base64_Entero;
    public $Base64;
    public $Extension;
    public $Nombre;
    public $Descripcion;
    public $Carpeta;
    public $Archivo;
    public $Datos;
    public $Size;

    function __construct($datos = null) {
        // $this->Modelo = 'App\Modelos\Cuenta';
        // $this->Datos = $datos['Datos'];
        if(empty($datos)) return;

        $this->Base64_Entero = $datos['Base64'];
        $this->Base64 = explode(',', $this->Base64_Entero)[1];

        $this->Archivo = $datos['Datos']['Archivo'];
        $this->Nombre = isset($datos['Datos']['Nombre']) ? $datos['Datos']['Nombre']: '';
        $this->Descripcion = isset($datos['Datos']['Descripcion']) ? $datos['Datos']['Descripcion']: '';

        $this->Carpeta = $datos['Carpeta'];
        $this->Datos = $datos['Datos'];

        $this->Extension = explode('.', $this->Archivo);
        $this->Extension = $this->Extension[count($this->Extension) - 1];   
    }

    public static function Form($base64, $nombre) {
        $UP = new Upload();
        $UP->Base64_Entero = $base64;
        $UP->Base64 = $base64;
        // $UP->Base64 = explode(',', $UP->Base64_Entero)[1];
        $UP->Archivo = $nombre;
        $UP->Extension = explode('.', $UP->Archivo);
        $UP->Extension = $UP->Extension[count($UP->Extension) - 1];   
        return $UP;
    }

    function Guardar($nombre, $carpeta = null) {
        // if(!empty($ArchivoGuardado)) {
        if(empty($carpeta))
            $Dir = $this->Carpeta;
        else
            $Dir = $carpeta;
        if(!is_dir($Dir)) mkdir($Dir, 0777, true);
        $Ruta = $Dir . '/' . $nombre . '.' . $this->Extension;	
        if(file_put_contents($Ruta, base64_decode($this->Base64))) {
            $this->Size = filesize($Ruta);
            return true;
        }
        return false;
    }
}
