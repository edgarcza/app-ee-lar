<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Base
{
    protected $table = 'users_devices';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            // ['users', 'users.id', 'posts.user_id']
        ];
    }
}
