<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostMedia extends Base
{
    protected $table = 'posts_media';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            ['posts', 'posts.id', 'posts_media.post_id'],
        ];
    }
}
