<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use App\ByL;
use App\Models\MessageGroup;
use App\Models\MessagesGroupMember;

class User extends Authenticatable
{
    protected $guarded = ['id'];
    protected $hidden = ['password'];
    public $errorCode = 0;
    public $errorMessage = 'Error';
    public $user_id = null;

    static function RT($rt)
    {
        return User::where('remember_token', '=', $rt)->first();
    }

    public function Register($user)
    {
        // $Created = User::where('email', '=', $user['email'])->exists();
        if (User::where('email', '=', $user['email'])->exists()) {
            $this->errorCode = 1;
            $this->errorMessage = 'El correo ya tiene una cuenta registrada';
            $this->user_id = User::where('email', '=', $user['email'])->first()->id;
            return false;
        }

        $User = User::create([
            'email' => $user['email'],
            'password' => Hash::make($user['password']),
            'firstName' => $user['firstName'],
            'lastName' => $user['lastName'],
            'name' => $user['name'],
            'birthday' => $user['birthday']
        ]);
        if ($User->exists) {
            $UserNots = NotificationConfig::create(['user_id' => $User->id]);
            // Buscar sus datos de cliente en ByL
            $ByLResponse = ByL::ClientByMail($User->email);
            if (count($ByLResponse['resourceList']) > 0) {
                // Si tiene datos (es cliente), crear un usuario a su seller
                $ByLResponse['resourceList'][0]['seller'];
                $SellerUser = new User();
                $SellerUser->Register([
                    'email' => $ByLResponse['resourceList'][0]['seller']['email'],
                    'password' => Hash::make('123'),
                    'firstName' => $ByLResponse['resourceList'][0]['seller']['name'],
                    'lastName' => $ByLResponse['resourceList'][0]['seller']['name'],
                    'name' => $ByLResponse['resourceList'][0]['seller']['name'],
                    'birthday' => '00-00-0000'
                ]);
                
                // Crear un messagegroup con el cliente y el seller
                $MessageGroup = MessageGroup::create([
                    'mg_first_user_id' => $User->id,
                    'mg_second_user_id' => $SellerUser->user_id,
                ]);

                // Agregar miembros al grupo
                MessagesGroupMember::create([
                    'message_group_id' => $MessageGroup->id,
                    'user_id' => $SellerUser->user_id,
                ]);
                MessagesGroupMember::create([
                    'message_group_id' => $MessageGroup->id,
                    'user_id' => $User->id,
                ]);
            }



            $this->user_id = $User->id;
            return true;
        }
        return false;
    }
}
