<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Base
{
    protected $table = 'posts';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            ['users', 'users.id', 'posts.user_id']
        ];
    }
}
