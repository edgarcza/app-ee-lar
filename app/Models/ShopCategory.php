<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopCategory extends Base
{
    protected $table = 'shop_categories';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            // ['shop_categories', 'shop_categories.id', 'shop_items.shop_category_id']
        ];
    }
}
