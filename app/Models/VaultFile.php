<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VaultFile extends Base
{
    protected $table = 'vault_files';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            ['users', 'users.id', 'vault_files.user_id']
        ];
    }
}
