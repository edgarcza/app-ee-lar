<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopItem extends Base
{
    protected $table = 'shop_items';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            ['shop_categories', 'shop_categories.id', 'shop_items.shop_category_id']
        ];
    }
}
