<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageState extends Base
{
    protected $table = 'messages_states';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            // ['users', 'users.id', 'messages_groups.mg_last_message_user_id'],
            // ['messages_groups', 'messages_groups.id', 'messages_groups_members.message_group_id'],
        ];
    }
}
