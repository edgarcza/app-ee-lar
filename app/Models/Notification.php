<?php

namespace App\Models;

use App\Push;
use Illuminate\Database\Eloquent\Model;

class Notification extends Base
{
    protected $table = 'notifications';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            // ['users', 'users.id', 'posts.user_id']
        ];
    }

    static function Send($userid, $type, $title, $body)
    {
        // $User = User::RT(($data['rt']));

        Notification::create([
            'user_id' => $userid,
            'title' => $title,
            'body' => $body,
            'date' => date('Y-m-d')
        ]);

        if (Notification::Able($type, $userid)) {
            $Not = new Push($userid);
            $Not->send($title, $body);
        }
    }

    static function Able($type, $userid)
    {
        $UserNots = NotificationConfig::where('user_id', '=', $userid)->first();
        $c_type = 'nc_' . $type;
        if ($UserNots[$c_type] > 0)
            return true;
        return false;
    }
}
