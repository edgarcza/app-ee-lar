<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPaymethod extends Base
{
    protected $table = 'users_paymethods';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            ['users', 'users.id', 'users_paymethods.user_id']
        ];
    }
}
