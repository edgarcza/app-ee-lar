<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Agente extends Base
{
    protected $table = 'agentes';
    protected $guarded = ['id'];

    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->Joins = [];
    }
    
}
