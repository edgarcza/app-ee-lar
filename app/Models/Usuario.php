<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Mail;
use App\Mail\Registro as RegistroMail;
use Illuminate\Support\Facades\Hash;

class Usuario extends Authenticatable
{
    public $Datos;
    // protected $fillable = ['nombres', 'apellidos', 'email', 'password'];
    protected $guarded = ['id'];

    public function Registrar($Usuario) {
        // $Usuario = $request->all()['datos'];

        $id_rol = (isset($Usuario['id_rol'])) ? $Usuario['id_rol'] : 4;
        $id_datos = (isset($Usuario['id_datos'])) ? $Usuario['id_datos'] : 1;
        // $password = (isset($Usuario['password'])) ? Hash::make($Usuario['id_datos']) : '';
        $password_token = Hash::make($Usuario['email']);

        $Creado = Usuario::create([
            'email' => $Usuario['email'],
            // 'password' => $password,
            'password_token' => $password_token,
            'id_rol' => $id_rol,
            'id_datos' => $id_datos,
        ]);

        if(!empty($Creado)) {
            $this->Datos = $Creado;

            // Enviar correo de creado
            Mail::to($Creado->email)->send(new RegistroMail($Creado));

            return true;
        }
        return false;
    }

}
