<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopCart extends Base
{
    protected $table = 'shop_carts';
    protected $guarded = ['id'];

    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->Joins = [
            ['shop_items_details', 'shop_items_details.id', 'shop_carts.shop_item_details_id'],
            ['shop_items', 'shop_items.id', 'shop_items_details.shop_id'],
            ['shop_categories', 'shop_categories.id', 'shop_items.shop_category_id']
        ];
    }
}
