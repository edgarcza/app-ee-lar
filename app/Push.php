<?php

namespace App;

use App\Models\UserDevice;

class Push
{
    private $token = null;
    private $user_id = null;
    private $devices = array();
    function __construct($user_id)
    {
        $this->user_id = $user_id;
        $UserDevices = UserDevice::where('user_id', '=', $user_id)->get();
        foreach ($UserDevices as $i => $Device) {
            $this->devices[$i] = $Device['nToken'];
        }
    }

    function send($title, $body)
    {
        $Expo = \ExponentPhpSDK\Expo::normalSetup();

        foreach ($this->devices as $i => $token) {
            $ExpoDetails = [$token, $token];
            $Expo->subscribe($ExpoDetails[0], $ExpoDetails[1]);
            $Notif = [
                'title' => $title,
                'body' => $body
            ];
            $Expo->notify($ExpoDetails[0], $Notif);
        }
    }
}
