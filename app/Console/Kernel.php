<?php

namespace App\Console;

use App\Models\Post;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        // $schedule->call(function () {
        //     Post::create([
        //         'post_content' => 'prueba',
        //         'post_date' => date('Y-m-d'),
        //         'user_id' => 1
        //     ]);
        // })->everyFiveMinutes();
        // $schedule->call('App\Http\Controllers\InfoController@Prueba')->everyMinute();
        // $schedule->call('App\Http\Controllers\InfoController@Push')->cron('0 0,2,4,6,8,12,14,16,18,20,22 * * * *');
        // $schedule->call('App\Http\Controllers\InfoController@Push')->cron('0 0,2,4,6,8,12,14,16,18,20,22 * * * *');
        $schedule->call('App\Http\Controllers\InfoController@Push')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
