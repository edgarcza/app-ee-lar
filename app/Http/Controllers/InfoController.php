<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Info;
use App\Models\User;
use App\ByL;
use App\Models\Notification;
use App\Models\Post;
use App\Push;

class InfoController extends BaseController
{
    function __construct()
    {
        $this->Modelo = 'App\Models\Info';
    }

    function Push()
    {
        // Obtener todos los usuarios que no sean tutores
        $Users = User::where('users.user_tutor_id', '=', 0)->get();
        $elarray = array();
        foreach ($Users as $i => $User) {
            $Client = ByL::ClientByMail($User['email']);
            // $elarray[$i]['user'] = $Client;
            foreach ($Client['resourceList'] as $j => $Sale) {
                $SaleData = ByL::SaleById($Sale['id']);
                $elarray[$i][$j]['user_id'] = $User['id'];
                $City = $SaleData['saleOrder']['institute']['city'];
                $elarray[$i][$j]['city'] = $City;

                $Client = new \GuzzleHttp\Client();
                $Request = $Client->request('GET', 'https://api.openweathermap.org/data/2.5/weather?q='.$City['name'].','.$City['countryCode'].'&appid=142998ac73ddaa4ac10b83fb737b9f8c&units=metric&lang=es');
                $Clima = json_decode($Request->getBody(), true);
                $elarray[$i][$j]['weather'] = $Clima;

                $InfoCreated = Info::create([
                    'user_id' => $User['id'],
                    'type' => 1,
                    'date' => date('Y-m-d'),
                    'content' => round($Clima['main']['temp']),
                    'content_2' => round($Clima['main']['feels_like']),
                    'content_3' => round($Clima['main']['humidity']),
                    'content_4' => $Clima['weather'][0]['description'],
                    'content_5' => date('d/m/Y G:i', $Clima['dt'] + $Clima['timezone']),
                    'image' => 'http://openweathermap.org/img/wn/'.$Clima['weather'][0]['icon'].'@2x.png',
                ]);

                // $Not = new Push($User['id']);
                Notification::Send($User['id'], 'weather', 'Clima en tu ciudad', 'El clima es de '.$InfoCreated->content.' con sensación de '.$InfoCreated->content_2);

                break;
            }
            // if ($i > 0)
            //     break;
            //['resourceList'][0]['client']
        }
        return response()->json(['error' => false, 'data' => $elarray]);
    }

    public function InfoUser(Request $request)
    {
        $data = $request->all();

        $User = User::RT($data['rt']);
        $Infos = Info::where('user_id', '=', $User->id)->orderBy('id', 'DESC')->get();

        return response()->json(['error' => false, 'data' => $Infos]);
    }

    function Prueba()
    {
        Post::create([
            'post_content' => 'prueba info',
            'post_date' => date('Y-m-d'),
            'user_id' => 1
        ]);
    }
}
