<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use App\ByL;
use App\Models\NotificationConfig;
use App\Models\UserDevice;
use App\Push;
use Illuminate\Support\Facades\Hash;
use App\Models\Upload;

class UserController extends BaseController
{
    public function Prueba()
    {
        // $pdf = PDF::loadHTML('<h1>Test</h1>');
        // return $pdf->stream();
        // return Excel::download(new PruebaExcel, 'prueba.xlsx');

        $client = new \GuzzleHttp\Client();
        $req = $client->request('GET', 'https://server.bookandlearn.com/masterkey/agency/sale/666668', [
            'headers' => [
                'X-Auth-Tenant' => 'embajadores',
                'X-Auth-Token' => 'B2FTChqGHobwQwmsdJ6uwxHBGzTewc55'
            ]
        ]);
        $res = $req->getBody();
        // print_r($res);
        echo $res;
        // return response()->json(['proceso' => true, 'datos' => ['asd' => 'sdad']]);
    }

    public function Create(Request $request)
    {
        $data = $request->all();
        $User = new User();
        if ($User->Register($data)) {
            return response()->json(['error' => false, 'data' => $data]);
        }
        return response()->json(['error' => true, 'data' => $data, 'errorMessage' => $User->errorMessage]);
    }

    public function Save(Request $request)
    {
        $data = $request->all();
        $User = User::RT(($data['rt']));
        unset($data['rt']);
        $User->fill($data);

        if (isset($data['tutor_app'])) {
            // Verificar si ya tenía usuario
            if (!User::where('id', '=', $User->tutor_user_id)->exists()) {
                // El tutor no tiene usuario, crearle uno
                $TUser = new User();
                $TUser->Register([
                    'email' => $User->tutor_email,
                    'password' => Hash::make($User->password),
                    'firstName' => $User->tutor_name,
                    'lastName' => $User->tutor_name,
                    'name' => $User->tutor_name,
                    'birthday' => $User->birthday
                ]);
                $User->tutor_user_id = $TUser->user_id;
            }
            // Aqui ya tiene usuario el tutor
            $TutorUser = User::where('id', '=', $User->tutor_user_id)->first();
            $TutorUser->user_tutor_id = $User->id;
            if ($data['tutor_app'] > 0) {
                // Permitir acceso
                $TutorUser->enabled = 1;
            } else {
                $TutorUser->enabled = 0;
            }
            $TutorUser->save();
        }

        if ($User->save()) {
            return response()->json(['error' => false, 'data' => $User]);
        }
        return response()->json(['error' => true, 'data' => $data]);
    }

    public function Login(Request $request)
    {
        $User = $request->all();
        if (Auth::attempt($User, true)) {
            // $AuthUser = Auth::user();
            return response()->json(['error' => false, 'data' => Auth::user()]);
        }
        return response()->json(['error' => true, 'data' => $User]);
    }

    public function FB(Request $request)
    {
        $Data = $request->all();
        $auth = ['email' => $Data['email'], 'password' => ''];

        if (!User::where('email', '=', $Data['email'])->exists()) {
            User::create([
                'email' => $Data['email'],
                'fb_token' => $Data['fb_token'],
                'firstName' => $Data['firstName'],
                'lastName' => $Data['lastName'],
                'name' => $Data['name'],
                'birthday' => $Data['birthday'],
                'password' => Hash::make('')
            ]);
        }

        if (Auth::attempt($auth, true)) {
            // $AuthUser = Auth::user();
            return response()->json(['error' => false, 'data' => Auth::user()]);
        }
        return response()->json(['error' => true, 'data' => $Data]);
    }

    public function Session(Request $request)
    {
        $rt = $request->all()['remember_token'];
        if (!empty($rt)) {
            $User = User::where('remember_token', $rt)->first();
            if ($User)
                return response()->json(['error' => false, 'data' => $User]);
        }
        return response()->json(['error' => true, 'data' => $rt]);
    }

    public function Avatar(Request $request)
    {
        $Avatar = $request->all()['avatar'];
        $RT = $request->all()['rt'];
        $User = User::RT($RT);
        $Up = Upload::Form($Avatar['base64'], $Avatar['uri']);
        $AvName = $User->id . '-' . strval(time());
        if ($Up->Guardar($AvName, 'media/avatars')) {
            $url = 'https://app.estudiantesembajadores.com/app-ee/public/media/avatars/' . $AvName . '.' . $Up->Extension;
            $User->avatar = $url;
            $User->save();
            return response()->json(['error' => false, 'data' => $User]);
        }
        return response()->json(['error' => true, 'data' => $Avatar]);
    }

    public function Get(Request $request)
    {
        $rt = $request->all()['rt'];
        $User = User::where('remember_token', '=', $rt)->first();

        return response()->json(['error' => false, 'data' => $User]);
    }

    public function ByL(Request $request)
    {
        $rt = $request->all()['rt'];
        $User = User::where('remember_token', '=', $rt)->first();
        $Response = ByL::ClientByMail($User->email);
        //['resourceList'][0]['client']
        return response()->json(['error' => false, 'data' => $Response['resourceList'][0]['client']]);
    }

    public function Sales(Request $request)
    {
        $rt = $request->all()['rt'];
        $User = User::where('remember_token', '=', $rt)->first();
        $Response = ByL::ClientByMail($User->email);
        $Sales = array();
        foreach ($Response['resourceList'] as $i => $Sale) {
            $ByL = new ByL();
            $Res = $ByL->Get('https://server.bookandlearn.com/masterkey/agency/sale/' . $Sale['id'] . '/quote');
            $Sales[$i] = $Res['resourceList'][0];
        }
        return response()->json(['error' => false, 'data' => $Sales]);
    }

    public function Device(Request $request)
    {
        $data = $request->all();
        $User = User::RT(($data['rt']));
        $UserDevice = UserDevice::create([
            'user_id' => $User->id,
            'nToken' => $data['nToken']
        ]);

        return response()->json(['error' => true, 'data' => $UserDevice]);
    }

    public function Not(Request $request)
    {
        $data = $request->all();
        $User = User::RT(($data['rt']));

        $Not = new Push($User->id);
        $Not->send('titulo', 'el cuerpo');

        return response()->json(['error' => true, 'data' => 'a']);
    }

    public function NotsConfig(Request $request)
    {
        $rt = $request->all()['rt'];
        $User = User::RT($rt);
        $Nots = NotificationConfig::where('user_id', '=', $User->id)->first();

        return response()->json(['error' => false, 'data' => $Nots]);
    }

    public function NotsConfigSave(Request $request)
    {
        $rt = $request->all()['rt'];
        $nots = $request->all()['nots'];
        $User = User::RT($rt);
        $Nots = NotificationConfig::where('user_id', '=', $User->id)->first();
        $Nots->fill([
            'nc_weather' => $nots['nc_weather'],
            'nc_city' => $nots['nc_city'],
            'nc_message' => $nots['nc_message'],
            'nc_comment' => $nots['nc_comment'],
            'nc_like' => $nots['nc_like'],
        ]);
        $Nots->save();
        return response()->json(['error' => false, 'data' => $Nots]);
    }
}
