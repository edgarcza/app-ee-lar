<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Agente as Modelo;
use App\Modelos\Usuario;

class AgenteController extends BaseController
{
    function __construct() {
        $this->Modelo = 'App\Modelos\Agente';
    }

    public function Guardar2(Request $request) {
        $Datos = $request->all()['datos'];

        // $Usuario;
        // $Agente;
        $Error = 0;

        if(!empty($Datos['agente']['id'])) {
            // Ya existe
            // unset($Datos['agente']['id_distribuidor']);
            $Agente = Modelo::find($Datos['agente']['id']);
            $Agente->fill($Datos['agente']);
            $Agente->save();
            $Usuario = Usuario::where('email', '=', $Datos['agente']['ag_correo_electronico'])->first();
        }
        else {
            $YaExiste = Usuario::where('email', '=', $Datos['agente']['ag_correo_electronico'])->first();
            if(!empty($YaExiste))
                return response()->json(['proceso' => false, 'datos' => $Datos, 'usuario' => $YaExiste, 'error' => 1]);

            // unset($Datos['agente']['id_distribuidor']);
            $Agente = Modelo::create($Datos['agente']);
            // Crearle usuario
            $Usuario = new Usuario();
            if($Usuario->Registrar([
                'email' => $Datos['agente']['ag_correo_electronico'], 
                'id_datos' => $Agente->id, 
                'id_rol' => $Datos['id_rol']
            ])) {
                $Usuario = $Usuario->Datos;
            }
        }

        return response()->json(['proceso' => true, 'datos' => $Agente, 'usuario' => $Usuario, 'error' => $Error]);
    }
}
