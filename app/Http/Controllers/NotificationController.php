<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\User;

class NotificationController extends BaseController
{
    function __construct()
    {
        $this->Modelo = 'App\Models\Notification';
    }

    public function NotsUser(Request $request)
    {
        $data = $request->all();

        $User = User::RT($data['rt']);
        $Infos = Notification::where('user_id', '=', $User->id)->orderBy('id', 'DESC')->get();

        return response()->json(['error' => false, 'data' => $Infos]);
    }
}
