<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MessagesGroupMember;

class MessagesGroupMemberController extends BaseController
{
    function __construct()
    {
        $this->Modelo = 'App\Models\MessagesGroupMember';
    }
}
