<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MessagesGroup;

class MessagesGroupController extends BaseController
{
    function __construct()
    {
        $this->Modelo = 'App\Models\MessagesGroup';
    }
}
