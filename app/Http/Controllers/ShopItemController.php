<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ShopItem;
use App\Models\ShopCategory;
use App\Models\ShopItemDetail;
use App\Models\ShopCart;

class ShopItemController extends BaseController
{
    function __construct()
    {
        $this->Modelo = 'App\Models\ShopItem';
    }

    public function LoadView(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);

        $Categories = ShopCategory::all();
        $ic = 0;
        foreach ($Categories as $i => $Category) {
            $Categories[$i]['items'] = ShopItem::where('shop_category_id', '=', $Category['id'])->get();
        }
        
        return response()->json(['error' => false, 'data' => $Categories]);
    }

    public function ItemDetails(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $ItemDetails = ShopItemDetail::where('shop_item_id', '=', $data['id'])->groupBy('shd_size')->get();   
        foreach ($ItemDetails as $i => $ItemDetail) {
            $ItemDetails[$i]['colors'] = ShopItemDetail::select('id as main_id', 'shd_color', 'shd_color_rgb')
                ->where('shop_item_id', '=', $ItemDetail['shop_item_id'])
                ->where('shd_size', '=', $ItemDetail['shd_size'])
                ->get();
        }
        
        return response()->json(['error' => false, 'data' => $ItemDetails]);
    }

    public function AddToCart(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $CartItem = ShopCart::create([
            'user_id' => $User->id,
            'shop_item_details_id' => $data['id'],
            'shc_status' => 1,
        ]);
        if(!empty($CartItem)) {
            return response()->json(['error' => false, 'data' => $CartItem]);
        }
        
        return response()->json(['error' => true, 'data' => $data]);
    }

    public function CartItems(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $CartItems = ShopCart::where('shc_status', '=', 1)
            ->where('user_id', '=', $User->id)
            ->join('shop_items_details', 'shop_carts.shop_item_details_id', 'shop_items_details.id')
            ->join('shop_items', 'shop_items_details.shop_item_id', 'shop_items.id')
            ->select('*', 'shop_carts.id AS main_id')
            ->get();
        
        return response()->json(['error' => false, 'data' => $CartItems]);
    }

    public function CartItemDelete(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        ShopCart::where('id', '=', $data['id'])->delete();
        $CartItems = ShopCart::where('shc_status', '=', 1)
            ->where('user_id', '=', $User->id)
            ->join('shop_items_details', 'shop_carts.shop_item_details_id', 'shop_items_details.id')
            ->join('shop_items', 'shop_items_details.shop_item_id', 'shop_items.id')
            ->select('*', 'shop_carts.id AS main_id')
            ->get();
        
        return response()->json(['error' => false, 'data' => $CartItems]);
    }

    
}
