<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PostComment;

class PostCommentController extends BaseController
{
    function __construct()
    {
        $this->Modelo = 'App\Models\PostComment';
    }
}
