<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    public $Modelo;
    // public $Joins;
    
    public function Save(Request $request) {
        $Guardar = $request->all()['datos'];
        $Modelo = new $this->Modelo;
        if($Modelo->Guardar($Guardar))
            return response()->json(['proceso' => true, 'datos' => $Modelo->Datos]);
        return response()->json(['proceso' => false, 'datos' => $Guardar]);
    }

    public function Delete(Request $request) {
        $ID = $request->all()['datos'];
        $Modelo = new $this->Modelo;
        if($Modelo->Eliminar($ID))
            return response()->json(['proceso' => true, 'datos' => $Modelo->Datos]);
        return response()->json(['proceso' => false, 'datos' => $ID]);

    }
    
    public function Filter(Request $request) {
        $Datos = $request->all()['datos'];
        $Modelo = new $this->Modelo;
        // $Filas = $Modelo->Filtrar($Datos);
        $Filas = $Modelo->Filtrar($Datos['Filtros'],
            isset($Datos['Paginador']) ? $Datos['Paginador'] : null);

        $Excel = null;
        if(isset($Datos['Excel']) && !empty($Datos['Excel'])) {
            $Excel = $Modelo->Excel($Datos['Excel']['Campos'], $Filas);
        }

        return response()->json(['proceso' => true, 'datos' => $Filas, 'total' => $Modelo->F_Datos_Total, 'req' => $Datos, 'joins' => $Modelo->Joins, 'excel' => $Excel]);
    }
    
    public function All(Request $request) {
        
        $Datos = $this->Modelo::all();
        // $Datos = $Query->get();

        return response()->json(['proceso' => true, 'datos' => $Datos]);
    }
}
?>