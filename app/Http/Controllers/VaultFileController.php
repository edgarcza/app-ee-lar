<?php

namespace App\Http\Controllers;

use App\Models\Upload;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\VaultFile;

class VaultFileController extends BaseController
{
    function __construct()
    {
        $this->Modelo = 'App\Models\VaultFile';
    }

    public function Files(Request $request)
    {
        $RT = $request->all()['rt'];
        $User = User::RT($RT);
        $VFiles = VaultFile::where('user_id', '=', $User->id)->get();
        return response()->json(['error' => true, 'data' => $VFiles]);
    }

    public function File(Request $request)
    {
        $File = $request->all()['file'];
        $RT = $request->all()['rt'];
        $User = User::RT($RT);
        $Up = Upload::Form($File['base64'], $File['uri']);
        $AvName = $User->id . '-' . strval(time());
        if ($Up->Guardar($AvName, 'media/vault_files')) {
            $url = 'https://app.estudiantesembajadores.com/app-ee/public/media/vault_files/' . $AvName . '.' . $Up->Extension;
            $VaultFile = VaultFile::create([
                'user_id' => $User->id,
                'v_url' => $url,
                'v_size' => $Up->Size,
                'v_name' => $Up->Extension,
                'v_date' => date('Y-m-d'),
            ]);
            $VFiles = VaultFile::where('user_id', '=', $User->id)->get();
            return response()->json(['error' => false, 'data' => $VFiles, 'vf' => $VaultFile]);
        }
        return response()->json(['error' => true, 'data' => $File]);
    }
}
