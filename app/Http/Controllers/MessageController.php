<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Message;
use App\Models\MessageGroup;
use App\Models\MessagesGroupMember;
use App\Models\MessageState;
use App\Models\Notification;

class MessageController extends BaseController
{
    function __construct()
    {
        $this->Modelo = 'App\Models\Message';
    }

    public function Messages(Request $request)
    {
        $rt = $request->all()['rt'];
        $User = User::RT($rt);
        $Messages = MessageGroup::join('messages_groups_members', 'messages_groups.id', 'messages_groups_members.message_group_id')
            ->where('messages_groups_members.user_id', '=', $User->id)->get();
        foreach ($Messages as $i => $Message) {
            $MessageUserId = null;
            if ($Message['mg_first_user_id'] > 0 && $Message['mg_first_user_id'] != $User->id)
                $MessageUserId = $Message['mg_first_user_id'];
            else if ($Message['mg_second_user_id'] > 0 && $Message['mg_second_user_id'] != $User->id)
                $MessageUserId = $Message['mg_second_user_id'];
            if (!empty($MessageUserId)) {
                $UserData = User::where('id', '=', $MessageUserId)->first();
                $Messages[$i]['mg_avatar'] = $UserData->avatar;
                $Messages[$i]['mg_name'] = $UserData->name;
            }
        }
        return response()->json(['error' => false, 'data' => $Messages]);
    }

    public function Chat(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $Chat = Message::where('message_group_id', '=', $data['chat'])->orderBy('m_timestamp', 'DESC')->get();
        foreach ($Chat as $i => $Message) {
            $Chat[$i]['type'] = $Message['user_id'] == $User->id ? 1 : 2;
            MessageState::where('message_id', '=', $Message['id'])
                ->where('ms_user_id', '=', $User->id)
                ->update(['ms_state' => 3]);
        }
        return response()->json(['error' => false, 'data' => $Chat]);
    }

    public function ChatNew(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $Chat = Message::join('messages_states', 'messages.id', 'messages_states.message_id')
            ->where('message_group_id', '=', $data['chat'])->orderBy('m_timestamp', 'DESC')
            ->where('ms_user_id', '=', $User->id)
            ->where('ms_state', '=', 2)
            ->select('messages.*')
            ->get();
        foreach ($Chat as $i => $Message) {
            $Chat[$i]['type'] = $Message['user_id'] == $User->id ? 1 : 2;
            MessageState::where('message_id', '=', $Message['id'])
                ->where('ms_user_id', '=', $User->id)
                ->update(['ms_state' => 3]);
        }
        return response()->json(['error' => false, 'data' => $Chat]);
    }

    public function Send(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $Message = Message::create([
            'message' => $data['message'],
            'user_id' => $User->id,
            'message_group_id' => $data['chat'],
            'm_date' => date('Y-m-d'),
        ]);

        $ChatMembers = MessagesGroupMember::where('message_group_id', '=', $data['chat'])->get();
        foreach ($ChatMembers as $i => $Member) {
            $State = MessageState::create([
                'ms_state' => $Member['user_id'] != $User->id ? 2 : 3,
                'message_id' => $Message->id,
                'ms_user_id' => $Member['user_id']
            ]);
            if($Member['user_id'] != $User->id)
                Notification::Send($Member['user_id'], 'message', 'Nuevo mensaje', $User->name . ': ' . $data['message']);
        }

        MessageGroup::where('id', '=', $data['chat'])
            ->update([
                'mg_last_message' => $data['message'],
                'mg_last_message_user_id' => $User->id,
            ]);

        $Chat = Message::where('message_group_id', '=', $data['chat'])->orderBy('m_timestamp', 'DESC')->get();
        foreach ($Chat as $i => $Message) {
            $Chat[$i]['type'] = $Message['user_id'] == $User->id ? 1 : 2;
        }
        return response()->json(['error' => false, 'data' => $Message, 'messages' => $Chat]);
    }
}
