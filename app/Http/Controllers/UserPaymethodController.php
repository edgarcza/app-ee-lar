<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserPaymethod;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserPaymethodController extends BaseController
{
    function __construct()
    {
        $this->Modelo = 'App\Models\UserPaymethod';
    }

    public function Get(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $Methods = UserPaymethod::where('user_id', '=', $User->id)->get();
        $UMethods = array();
        foreach ($Methods as $i => $Met) {
            // $UMethods[$i]['type'] = $Met;
            $UMethods[$i]['id'] = $Met['id'];
            $UMethods[$i]['type'] = $Met['data0'];
            $UMethods[$i]['owner'] = decrypt($Met['data1']);
            $UMethods[$i]['number'] = 'X-' . substr(decrypt($Met['data2']), 12, 4);
            $UMethods[$i]['exp_month'] = decrypt($Met['data3']);
            $UMethods[$i]['exp_year'] = decrypt($Met['data4']);
        }

        return response()->json(['error' => false, 'data' => $UMethods]);
    }

    public function Add(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $Method = $data['pm'];
        if($data['type'] == 1) {
            $Created = UserPaymethod::create([
                'data0' => $data['type'],
                'data1' => encrypt($Method['owner']),
                'data2' => encrypt($Method['number']),
                'data3' => encrypt($Method['exp_month']),
                'data4' => encrypt($Method['exp_year']),
                'user_id' => $User->id
            ]);
            return response()->json(['error' => false, 'data' => $Created]);
        }

        return response()->json(['error' => false, 'data' => $data]);
    }

    public function DeleteMethod(Request $request)
    {
        $data = $request->all();
        $Deleted = UserPaymethod::where('id', '=', $data['id'])->delete();

        return response()->json(['error' => false, 'data' => $Deleted]);
    }
}
