<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Modelos\Usuario;
use App\Modelos\Persona;
use Auth;

class UsuarioController extends Controller
{
    public function Login(Request $request) {
        $Usuario = $request->all()['datos'];
        if(Auth::attempt($Usuario, true)) {
            $Usuario = Auth::user();
            return response()->json(['proceso' => true, 'usuario' => Auth::user()]);
        }
        else {
            return response()->json(['proceso' => false, 'usuario' => $Usuario]);
        }
    }

    public function Sesion(Request $request) {
        $Usuario = Usuario::where('remember_token', $request->all()['datos']['remember_token'])->first();
        if($Usuario)
            return response()->json(['proceso' => true, 'usuario' => $Usuario]);
        return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    //
    public function Registrar(Request $request) {
        $Usuario = $request->all()['datos'];

        $Creado = Usuario::create([
            'email' => $Usuario['email'],
            'password' => Hash::make($Usuario['password']),
        ]);

        if(!empty($Creado)) {

            $Persona = Persona::create([
                'nombres' => $Usuario['nombres'],
                'apellidos' => $Usuario['apellidos'],
                'id_usuario' => $Creado['id'],
                'rs_tipo' => $Usuario['rs_tipo'],
            ]);

            return response()->json(['proceso' => true, 'usuario' => $Creado, 'persona' => $Persona]);
        }
        return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    public function Guardar(Request $request) {
        $Datos = $request->all()['datos'];

        // $Usuario = Usuario::find($Datos['id']);
        // $Usuario
        // $Usuario->email = $Datos['email'];
        // $Usuario['password'] = Hash::make($Datos['password']);
        // $Usuario->save();
        // $Usuario;

        if(empty($Datos['password']))
            unset($Datos['password']);

        $Usuario = Usuario::find($Datos['id']);
        // $Usuario = new Usuario($Datos);
        // unset($Datos['nombres']);
        // unset($Datos['apellidos']);

        $Usuario->fill([
            'email' => $Datos['email'],
        ]);
        if(isset($Datos['password']))
            $Usuario->password = Hash::make($Datos['password']);
        $Usuario->save();

        $Persona = Persona::where('id_usuario', '=', $Usuario->id)->first();
        $Persona->nombres = $Datos['nombres'];
        $Persona->apellidos = $Datos['apellidos'];
        $Persona->rs_tipo = $Datos['rs_tipo'];
        $Persona->save();

        // if(!empty($Usuario))
            return response()->json(['proceso' => true, 'usuario' => $Usuario]);
        // return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    public function Eliminar(Request $request) {
        $ID = $request->all()['datos'];

        $Usuario = Usuario::where('id', $ID)->delete();
        // $Usuario;

        // if(!empty($Usuario))
            return response()->json(['proceso' => true, 'usuario' => $Usuario]);
        // return response()->json(['proceso' => false, 'usuario' => $Usuario]);
    }

    
    public function Usuarios(Request $request) {
        $Filtros = $request->all()['datos']['Filtros'];
        $Paginador = $request->all()['datos']['Paginador'];
        
        $Query = Usuario::query();
        foreach ($Filtros as $Filtro) {
            $Query = $Query->where($Filtro['Campo'], 'like', '%'.$Filtro['Valor'].'%');
        }
        // $Query = $Query->join('personas_tipo', 'usuarios.tipo', 'personas_tipo.id');
        $Query = $Query->join('personas', 'personas.id_usuario', 'usuarios.id');
        $Query = $Query->join('personas_tipo', 'personas.rs_tipo', 'personas_tipo.id');
        $DatosUL = $Query->get()->count();
        $Query = $Query->offset($Paginador['Cantidad'] * $Paginador['Pagina']);
        $Query = $Query->limit($Paginador['Cantidad']);
        $Datos = $Query->get();



        return response()->json(['proceso' => true, 'datos' => $Datos, 'total' => $DatosUL, 'req' => $request->all()['datos']['Paginador']]);
    }

    public function Todos() {        
        // $Datos = Usuario::all();
        $Query = Usuario::query();
        $Query->join('personas', 'usuarios.id', 'personas.id_usuario');
        $Datos = $Query->where('personas.rs_tipo', '=', 1)->get();

        return response()->json(['proceso' => true, 'datos' => $Datos]);
    }
}
