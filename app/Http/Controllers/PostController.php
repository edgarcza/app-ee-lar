<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostLike;
use App\Models\PostMedia;
use App\Models\User;
use App\Models\Upload;

class PostController extends BaseController
{
    function __construct()
    {
        $this->Modelo = 'App\Models\Post';
    }

    public function Create(Request $request)
    {
        $PostData = $request->all();
        $User = User::RT($PostData['rt']);
        $Post = Post::create([
            'post_content' => $PostData['content'],
            'post_date' => date('Y-m-d', strtotime($PostData['date'])),
            'user_id' => $User->id
        ]);

        $UpDatos = array();
        foreach ($PostData['media'] as $i => $Media) {
            $Up = Upload::Form($Media['base64'], $Media['uri']);
            if ($Up->Guardar($Media['id'], 'media/posts')) {
                $UpDatos[$i] = $Up->Size;
                PostMedia::create([
                    'pm_url' => 'http://app.estudiantesembajadores.com/app-ee/public/media/posts/' . $Media['id'] . '.' . $Up->Extension,
                    'post_id' => $Post->id,
                    'pm_size' => $Up->Size,
                ]);
            }
        }

        // return response()->json(['error' => false, 'data' => $User]);
        return response()->json(['error' => false, 'data' => $PostData, 'ex' => $UpDatos]);
    }

    public function Posts(Request $request)
    {
        $data = $request->all();
        $PostsObj = new Post();
        $PostsObj->Orders = [['post_timestamp', 'DESC']];
        $Posts = $PostsObj->Filter();

        $User = User::RT($data['rt']);
        foreach ($Posts as $i => $Post) {
            // $Posts[$i]['medias'] = $Post['id'];
            if (PostLike::where('post_id', '=', $Post['main_id'])->where('user_id', '=', $User->id)->count() > 0)
                $Posts[$i]['liked'] = true;
            else
                $Posts[$i]['liked'] = false;
            $Posts[$i]['media'] = PostMedia::where('post_id', '=', $Post['main_id'])->get();
        }

        return response()->json(['error' => false, 'data' => $Posts]);
    }

    public function PostsProfile(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $ProfileId = 0;
        if ($data['rt'] == $data['profile'])
            $ProfileId = $User->id;
        else {
            $ProfileId = $data['profile'];
        }
        $PostsObj = new Post();
        $PostsObj->Orders = [['post_timestamp', 'DESC']];
        $Posts = $PostsObj->Filter([
            ['field' => 'posts.user_id', 'value' => $ProfileId, 'cond' => '=']
        ]);

        foreach ($Posts as $i => $Post) {
            // $Posts[$i]['medias'] = $Post['id'];
            if (PostLike::where('post_id', '=', $Post['main_id'])->where('user_id', '=', $User->id)->count() > 0)
                $Posts[$i]['liked'] = true;
            else
                $Posts[$i]['liked'] = false;
            $Posts[$i]['media'] = PostMedia::where('post_id', '=', $Post['main_id'])->get();
        }

        return response()->json(['error' => false, 'data' => $Posts]);
    }

    public function Like(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $Post = Post::where('id', '=', $data['post_id'])->first();
        $PostLike = PostLike::where('post_id', '=', $Post->id)->where('user_id', '=', $User->id)->first();
        if (!empty($PostLike)) {
            $PostLike->delete();
        } else {
            PostLike::create([
                'pl_date' => date('Y-m-d'),
                'pl_timestamp' => time(),
                'post_id' => $Post->id,
                'user_id' => $User->id
            ]);
            if ($User->id != $Post->user_id)
                Notification::Send($Post->user_id, 'like', 'Nuevo like', 'A ' . $User->name . ' le gustó tu post');
        }
        $Res['post_likes'] = PostLike::where('post_id', '=', $Post->id)->count();
        $Post->post_likes = $Res['post_likes'];
        $Post->save();
        return response()->json(['error' => false, 'data' => $Res]);
    }

    public function Comment(Request $request)
    {
        $data = $request->all();
        $User = User::RT($data['rt']);
        $Post = Post::where('id', '=', $data['post_id'])->first();
        PostComment::create([
            'pc_date' => date('Y-m-d'),
            'pc_comment' => $data['comment'],
            'post_id' => $Post->id,
            'user_id' => $User->id
        ]);

        if ($User->id != $Post->user_id)
            Notification::Send($Post->user_id, 'comment', 'Nuevo comentario', $User->name . ' comentó tu post');

        $Res['post_comments'] = PostComment::where('post_id', '=', $Post->id)->count();
        $Post->post_comments = $Res['post_comments'];
        $Post->save();
        return response()->json(['error' => false, 'data' => $Res]);
    }

    public function Comments(Request $request)
    {
        $data = $request->all();
        // $User = User::RT($data['rt']);
        $PostsObj = new PostComment();
        // $PostsObj->Orders = [['post_timestamp', 'DESC']];
        $Comments = $PostsObj->Filter([
            ['field' => 'post_id', 'cond' => '=', 'value' => $data['post_id']]
        ]);
        // $Comments = PostComment::where('post_id', '=', $data['post_id'])->get();
        return response()->json(['error' => false, 'data' => $Comments]);
    }
}
