<?php

namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class Prueba implements FromView
{
    public function view(): View
    {
        return view('excel.prueba', [
            'datos' => [
                ['id' => 1, 'dato' => 'prueba 1'],
                ['id' => 2, 'dato' => 'prueba 2']
            ]
        ]);
    }
}