<?php
namespace App;

class ByL
{
    private $Tenant = 'embajadores';
    private $Token = 'B2FTChqGHobwQwmsdJ6uwxHBGzTewc55';
    private $Client;
    private $Options;

    function __construct()
    {
        $this->Client = new \GuzzleHttp\Client();
        $this->Options = [
            'headers' => [
                'X-Auth-Tenant' => $this->Tenant,
                'X-Auth-Token' => $this->Token
            ]
        ];
    }

    public function Get($link)
    {
        $Request = $this->Client->request('GET', $link, $this->Options);
        return json_decode($Request->getBody(), true);
    }

    static function ClientByMail($mail)
    {
        $Book = new ByL();
        $Response = $Book->Get('https://server.bookandlearn.com/masterkey/agency/sale?status=Approved&q='.$mail);
        return $Response;
    }

    static function SaleById($id)
    {
        $Book = new ByL();
        $Response = $Book->Get('https://server.bookandlearn.com/masterkey/agency/sale/'.$id);
        return $Response;
    }
}
